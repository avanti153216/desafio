# Desafio Terraform e Gitlab

## Desenvolvimento

1 - Iniciando o terraform:
 Na pasta 'terraform', digite o comando ```terraform init``` no terminal. Isto gerará o setup inicial do terraform no projeto.

![alt text](images/terraform-init.png)

 2 - Configuração do plano: 
  Utilize o comando ```terraform plan```  para verificar as ações que serão realizadas quando o plano for executado.

  ![alt text](images/terraform-plan.png)

3 - Aplicação:
 Utilize o comando ```terraform apply``` para aplicar o script;

4 - Verificar a instalação na plataforma da AWS:
No EC2, verifique se o runner têm as permissões necessárias utilizando o comando:

```ls -lha /var/www/ ```

![alt text](images/check-permission.png)

5 - Registre o runner do gitlab conforme o token gerado na plataforma do Gitlab:

```sudo gitlab-runner register --url https://gitlab.com/ --registration-token GR1348941QeyG6Ej_jaDDdGW_vzvz ```

![alt text](images/register-runner.png)

6 - Verificar se o runner do Gitlab está conectado com a máquina virtual:

![alt text](images/check-runner-gitlab.png)

6 - Verifique o Pipeline no GitLab:

![alt text](images/job-gitlab.png)

7 - Verifique se o deploy da aplicação está funcionando diretamente no IP gerado na AWS:

![alt text](images/deploy.png)


8 - Para prevenir gastos desnecessário nas AWS, destrua a máquina virtual se não estiver utilizando:

``` terraform destroy```

![alt text](images/terraform-destroy.png)

![alt text](images/destroy-confirm.png)